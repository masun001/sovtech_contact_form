package utilities;

import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import com.aventstack.extentreports.ExtentReports;

public class SetUpClass {
	
	//public static  WebDriver driver;
	 public Properties props;
	 public   String confgfile;
	 public XSSFSheet ContactForm_sheet;
	 public  XSSFWorkbook wk1;
	 public  ExtentReports extent;
	 public  String browser;
	 
	public WebDriver startBrowser(String browser,String Url) throws IOException {
		loadProperty();
		 
		/*System.setProperty(props.getProperty("Webdriver"), props.getProperty("DriverPath"));
	   driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(3000, TimeUnit.SECONDS);
		 */
		WebDriver driver = null;
		
		if (browser.equalsIgnoreCase("chrome")) {
			System.setProperty(props.getProperty("Webdriver"), props.getProperty("DriverPath"));
			driver = new ChromeDriver();
			driver.navigate().to(Url);
			driver.manage().window().maximize();
		   driver.manage().timeouts().implicitlyWait(3000, TimeUnit.SECONDS);
			 
			
		}
		else if (browser.equalsIgnoreCase("firefox")) {
			System.setProperty(props.getProperty("FirefoxDriver"),props.getProperty("FirefoxPath"));
			driver = new FirefoxDriver();
		
			driver.navigate().to(Url);
			driver.manage().window().maximize();
			driver.manage().timeouts().implicitlyWait(3000, TimeUnit.SECONDS);
			
			
			
		}
		//driver.navigate().to(Url);
		//driver.manage().timeouts().implicitlyWait(300, TimeUnit.SECONDS);
		
		  
		  return driver;
	}  
	
	public  XSSFSheet ContactForm11(Properties props) throws IOException {
		wk1 = new XSSFWorkbook(props.getProperty("SovTech_ContactForm"));
		  
		   return ContactForm_sheet=wk1.getSheet("Username");
	}
	
	
	public  XSSFSheet  ContactForm1(Properties props) throws IOException {
		wk1 = new XSSFWorkbook(props.getProperty("SovTech_ContactForm"));
	
		 
		   return ContactForm_sheet = wk1.getSheet("Email_Address");
	}
	
	public  XSSFSheet  ContactForm(Properties props) throws IOException {
		wk1 = new XSSFWorkbook(props.getProperty("SovTech_ContactForm"));
	
		   return ContactForm_sheet = wk1.getSheet("MobileNo");
	}
	public Properties loadProperty() throws IOException {
		 props = new Properties();
	    confgfile = "Driver_and_files\\config.properties";

	   FileReader fReader = new FileReader(confgfile);
	   props.load(fReader);
		return props;

	
			    
	}
}
