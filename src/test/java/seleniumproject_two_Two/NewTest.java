package seleniumproject_two_Two;

import org.testng.annotations.Test;

import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;


import application.Main_Class;


import reporting.Reporting;
import utilities.SetUpClass;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;

import java.io.IOException;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterMethod;

public class NewTest {
  
	 Main_Class set = new Main_Class();
	 Reporting re = new Reporting();
	
	 
	WebDriver driver;
	
	 @BeforeMethod
	 @Parameters({"browser","Url"})
	  public void beforeMethod(String browser,String Url) throws IOException {
		  driver = set.startBrowser(browser,Url);
		//  re.reportmethod();
		
		
	  }
	  
	@Test
  public void f() throws Exception {
		
		int rowCount=set.dataForm(set.loadProperty()).getPhysicalNumberOfRows();
		
		for (int i = 1; i < rowCount; i++) {
			 Cont_Form.form(i,driver,set.dataForm(set.loadProperty()),set.loadProperty());
			 
			 String title = driver.getTitle();
			 String ttt ="SOVTECH Contact Form";
			 if (title.contains(ttt) == true) {
			
				 re.verfyPage(driver);
			}
			 
			 
			
		}
		
		
	 
		
	 
  }
	
	

  @AfterMethod
  public  void afterMethod() throws Exception {
	 
	 
	  lo.quitDrive(driver);
  }

}
