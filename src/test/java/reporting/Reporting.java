package reporting;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

import utilities.SetUpClass;



public class Reporting {
	static ExtentHtmlReporter htmlReporter;
    static ExtentReports extent;
    static ExtentTest test;
    static WebDriver driver;
	
	SetUpClass set = new SetUpClass();

 public void reportmethod()
	    {
	        htmlReporter = new ExtentHtmlReporter("Report\\index.html");
	       htmlReporter.setAppendExisting(true);
	        
	        
	        extent = new ExtentReports();
	        extent.attachReporter(htmlReporter);
	       test = extent.createTest("SOVTECH System Test");
	        
	        test.assignAuthor("Nandi Masuku");
	    }

	   
	    public String takeScreenShoot(String filename,WebDriver driver)throws Exception{
	     

	       File srcFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
	       String timeStamp = new SimpleDateFormat("yyyymmdd_HHmmss").format(Calendar.getInstance().getTime());
	       filename= ".\\ScreenShot\\"+filename+ timeStamp + ".png";
	       
	       FileUtils.copyFile (srcFile,new File(filename));

	     return filename;
	     
	 
	    }
	   
	   
	 public ExtentTest verfyPage(WebDriver driver) throws Exception { 
		 
			// String title = SetUpClass.driver.getTitle();
	             reportmethod();
		 
	    	 String fileName = takeScreenShoot("/screenshoots",driver);
	         test.log(Status.FAIL, MarkupHelper.createLabel(" Test case Fail:", ExtentColor.RED));
	         test.pass("screenshoots",MediaEntityBuilder.createScreenCaptureFromPath("."+fileName).build());
	       
	     
	 
	     extent.flush();
	     return test;
	 }
		 public ExtentTest validvalues(WebDriver driver) throws Exception {
			
			 String fileName = takeScreenShoot("/screenshoots",driver);
	         test.log(Status.PASS, MarkupHelper.createLabel(" Test Case PASSED", ExtentColor.GREEN));
	         test.pass("screenshoots",MediaEntityBuilder.createScreenCaptureFromPath("."+fileName).build());
	         extent.flush();
	         return test;
		}
		
		
			
		

 }

	
	


