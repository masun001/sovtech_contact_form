package application;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;



public class Main_Class {

	public static String browser = "chrome";
	public static WebDriver driver;

	@SuppressWarnings("deprecation")
	public static void main(String[] args) throws InterruptedException {

		if (browser.equals("chrome")) {
			// WebDriverManager.firefox().setup();
			System.setProperty("webdriver.chrome.driver",
					"C:\\Users\\Nandi.Masuku\\eclipse-workspace\\SOVTECT1-ASSESSMENT\\driver\\chromedriver.exe");
			//Opening webbrowser 
			driver = new ChromeDriver();
			driver.get("https://www.sovtech.co.za/contact-us/");
			// implicit wait
			driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);

			driver.manage().window().maximize();
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			System.out.println("Page Launched");

			// VERIFY NAME FUNCTION WITH VALID AND INVALID TEST DATA ENTERED ON TEXTBOX
			driver.findElement(By.name("your_name")).sendKeys("N@nd!"); // invalid Name
			// Verify error message
			// expected error text
			String exp = "Please enter valid name";
			// identify actual error message
			WebElement m = driver.findElement(By.className("Please complete the required field"));
			String act = m.getText();
			System.out.println("Error message is: " + act);
			// verify error message with Assertion
			// Assert.assertEquals(exp, act);

			driver.findElement(By.id("your_name-c2e387f9-4bd8-496f-ab2a-81fbbc31712a")).clear(); // clear Name textbox

			// implicit wait
			driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
			Thread.sleep(1000);

			// report and screenshot of error message "Please complete the required field"
			driver.findElement(By.xpath("/html/body/div/form/div[1]/div/input")).sendKeys("Nandi"); // Valid Name
			Thread.sleep(2000);

			// VERIFY FORMAT OF EMAIL ADDRESS
			driver.findElement(By.xpath("/html/body/div/form/div[2]/div/input")).sendKeys("0000.co.za"); // invalid
																											// email
																											// address
			
			//driver.findElement(Email_Address)).sendKeys("0000.co.za"); // invalid email address (reading from configuration file)

			// Verify error message
			// expected error text
			String exp2 = "Email must be formatted correctly";
			// identify actual error message
			WebElement m2 = driver.findElement(By.className("Email must be formatted correctly"));
			String act2 = m2.getText();
			System.out.println("Error message is: " + act2);
			// verify error message with Assertion
			// Assert.assertEquals(exp2, act2);
			driver.findElement(By.xpath("/html/body/div/form/div[2]/div/input")).clear(); // clear email address textbox

			// report and screenshot of error message "Email must be formatted correctly�"
			driver.findElement(By.xpath("/html/body/div/form/div[2]/div/input")).sendKeys("nandimasuku@sovtech.co.za"); // Valid
																														// email
																														// address

			// VERIFY MOBILE PHONE FORMAT
			driver.findElement(By.xpath("/html/body/div/form/div[2]/div/input")).sendKeys("gjj235ghnb75"); // invalid
																											// mobile
																											// number
			// expected error text
			String exp3 = "Must contain only numbers, +()-.and x";
			// identify actual error message
			WebElement m3 = driver.findElement(By.className("Must contain only numbers, +()-.and x"));
			String act3 = m3.getText();
			System.out.println("Error message is: " + act3);
			// verify error message with Assertion
			// Assert.assertEquals(exp3, act3);

			driver.findElement(By.xpath("/html/body/div/form/div[2]/div/input")).clear(); // clear mobile number textbox

			// report and screenshot of error message "Must contain only numbers, +()-.and
			// x"
			driver.findElement(By.xpath("/html/body/div/form/div[3]/div/input")).sendKeys("+27 67 903 9488"); // Enter
																												// valid
																												// test
																												// data

			// VERIFY COMPANY SIZE DROPBOX FUNCTION
			driver.findElement(By.xpath("/html/body/div/form/div[4]/div/select")).click();
			Select drpCompSize = new Select(driver.findElement(By.xpath("/html/body/div/form/div[4]/div/select")));
			drpCompSize.selectByVisibleText("50-100");
			drpCompSize.selectByIndex(3);

			// HOW CAN WE HELP YOU� MESSAGE BOX FUNCTION
			driver.findElement(By.xpath("/html/body/div/form/div[5]/div/select")).click(); // Select an option from
																							// dropbox menu

			// report and screenshot to verify selected option
			// Type message on "how can we help you" textbox when selecting other
			Select drpHelp = new Select(driver.findElement(By.xpath("/html/body/div/form/div[5]/div/select")));
			drpHelp.selectByVisibleText("Scale engineering with managed teams");
			drpHelp.selectByIndex(2);

			// TYPE MESSAGE ON HOW_CAN_WE_HELP_YOU TEXTBOOK
			driver.findElement(By.xpath("/html/body/div/form/div[5]/div/select")).click();
			driver.findElement(By.xpath("/html/body/div/form/div[6]/div/textarea"))
					.sendKeys("Develop and test product with end-user");
			// report and screenshot

			// VERIFY CHECK BOX BUTTOM (I agree to receive other communications from
			// SovTech) AND SUBMIT BUTTON FUNCTION
			driver.findElement(By.xpath("/html/body/div/form/div[7]/div[2]/div/div/div/ul/li/label/input")).click();

			// Click submit button
			driver.findElement(By.xpath("/html/body/div/form/div[8]/div[2]/input")).click();
			// report and screenshot to verify message "Submission successful"

		}

	}

	public Object loadProperty() {
		// TODO Auto-generated method stub
		return null;
	}

}
